package pos.machine;

import java.util.*;
import java.util.stream.Collectors;

public class PosMachine {

    public String printReceipt(List<String> barcodes) {
        List<Item> items = ItemsLoader.loadAllItems();
        List<ReceiptItem> receiptItems = decodeToItems(barcodes, items);
        Receipt receipt = calculateCost(receiptItems);
        return renderReceipt(receipt);
    }

    public String renderReceipt(Receipt receipt) {
        String itemsReceipt = generateItemsReceipt(receipt);
        return generateReceipt(itemsReceipt, receipt.getTotalPrice());
    }

    public String generateReceipt(String itemsReceipt, Integer totalPrice) {
        return "***<store earning no money>Receipt***" +
                System.lineSeparator() +
                itemsReceipt + System.lineSeparator() + "----------------------" +
                System.lineSeparator() + "Total: " +
                totalPrice + " (yuan)" + System.lineSeparator() +
                "**********************";
    }

    //    Name: Coca-Cola, Quantity: 4, Unit price: 3 (yuan), Subtotal: 12 (yuan)
    public String generateItemsReceipt(Receipt receipt) {
        return receipt.getReceiptItems()
                .stream()
                .map(receiptItem ->
                        "Name: " + receiptItem.getName() +
                                ", Quantity: " + receiptItem.getQuantity() +
                                ", Unit price: " + receiptItem.getUnitPrice() + " (yuan)" +
                                ", Subtotal: " + receiptItem.getSubTotal() + " (yuan)"
                )
                .collect(Collectors.joining(System.lineSeparator()));
    }

    public List<ReceiptItem> decodeToItems(List<String> barcodes, List<Item> items) {
        Map<String, ReceiptItem> receiptMap = new LinkedHashMap<>();
        barcodes.forEach(barcode->{
            if (Objects.nonNull(receiptMap.get(barcode))) {
                ReceiptItem receiptItem = receiptMap.get(barcode);
                int quantity = receiptItem.getQuantity();
                receiptItem.setQuantity(++quantity);
            } else {
                ReceiptItem receiptItem = new ReceiptItem();
                receiptItem.setQuantity(1);
                Item targetItem = items.stream().filter(item -> item.getBarcode().equals(barcode)).findFirst().get();
                receiptItem.setName(targetItem.getName());
                receiptItem.setUnitPrice(targetItem.getPrice());
                receiptMap.put(barcode, receiptItem);
            }
        });

        return new ArrayList<>(receiptMap.values());
    }

    public Receipt calculateCost(List<ReceiptItem> receiptItems) {
        Receipt receipt = new Receipt();
        receipt.setReceiptItems(calculateItemsCost(receiptItems));
        receipt.setTotalPrice(calculateTotalPrice(receiptItems));
        return receipt;
    }

    public List<ReceiptItem> calculateItemsCost(List<ReceiptItem> receiptItems) {
        return receiptItems.stream().peek(receiptItem -> receiptItem.setSubTotal(receiptItem.getUnitPrice() * receiptItem.getQuantity())).collect(Collectors.toList());
    }

    public int calculateTotalPrice(List<ReceiptItem> receiptItems) {
        return receiptItems.stream().mapToInt(ReceiptItem::getSubTotal).reduce(0, Integer::sum);
    }
}
